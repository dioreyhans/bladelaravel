<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'] );
Route::get('/form', [AuthController::class, 'register'] );
Route::post('/register', [AuthController::class, 'welcome'] );
Route::get('master', function(){
    return view('adminlte/master');
} );

Route::get('table', function(){
    return view('items/index');
} );
Route::get('data-tables', function(){
    return view('items/data_tabel');
} );